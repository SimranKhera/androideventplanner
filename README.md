# AndroidEventPlanner

This Android application is used to create and edit events while being able to receive notifications for them.
When an event is created through this application, the event is also created into the calendar app.

It is assumed that the current user is logged into the calendar app.
