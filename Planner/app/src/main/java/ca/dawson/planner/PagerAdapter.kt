package ca.dawson.planner

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Changes fragments depending on the current tab.
 * @author Simranjot Kaur Khera
 */
class PagerAdapter(fm: FragmentManager, val context: Context): FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when(position){
            0 -> return EventListFragment(context)

        }
        return EventNewFragment(context)
    }

    override fun getCount(): Int {
       return 2
    }

}