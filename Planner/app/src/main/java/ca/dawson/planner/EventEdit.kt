package ca.dawson.planner

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.util.*

/**
 * Gets new data from users and changes event properties. The event is then updated in the
 * Calendar app by using the CRUD method for updates.
 * @author Simranjot Kaur Khera
 */
class EventEdit : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var  event: Event
    val c = Calendar.getInstance()
    private val eventManager: EventManager = EventManager(this)
    //start date
    var startYear = c.get(Calendar.YEAR)
    var startMonth = c.get(Calendar.MONTH)
    var startDay = c.get(Calendar.DAY_OF_MONTH)
    var startHour = c.get(Calendar.HOUR)
    var startMinute = c.get(Calendar.MINUTE)

    //end date
    var endYear = c.get(Calendar.YEAR)
    var endMonth = c.get(Calendar.MONTH)
    var endDay = c.get(Calendar.DAY_OF_MONTH)
    var endHour = c.get(Calendar.HOUR)
    var endMinute = c.get(Calendar.MINUTE)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_event)
        //getting event from intent
        val foundEvent: Event? = intent.extras!!.getSerializable("Event") as Event?
        if (foundEvent != null) {
            Log.i("EDIT", "Found event to edit: " + foundEvent.toString())
            this.event = foundEvent
        }

        //importance spinner
        val spinner: Spinner = findViewById(R.id.importanceSpinner)
        spinner.onItemSelectedListener = this

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.importance_list,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        setOriginalData()
    }

    private fun setOriginalData(){
        val startDateButton: Button = findViewById<Button>(R.id.startDateButton)
        val endDateButton: Button = findViewById<Button>(R.id.endDateButton)
        val startTimeButton: Button = findViewById<Button>(R.id.startTimeButton)
        val endTimeButton: Button = findViewById<Button>(R.id.endTimeButton)
        val descEditText: EditText = findViewById<EditText>(R.id.eventDescEditText)
        val titleEditText: EditText = findViewById<EditText>(R.id.eventTitleEditText)

        val endDate: String = this.event.getDate(this.event.endDate, "dd/MM/yyyy hh:mm")
        val endDateSpace: Int = endDate.indexOf(" ")
        val startDate: String = this.event.getDate(this.event.startDate, "dd/MM/yyyy hh:mm")
        val startDateSpace: Int = startDate.indexOf(" ")

        startDateButton.text = "Start Date: ${startDate.substring(0, startDateSpace)}"
        endDateButton.text = "End Date: ${endDate.substring(0, endDateSpace)}"
        startTimeButton.text = "Start time: ${startDate.substring(startDateSpace+1, startDate.length)}"
        endTimeButton.text = "Start time: ${endDate.substring(endDateSpace+1, endDate.length)}"

        descEditText.setText(this.event.desc)
        titleEditText.setText(this.event.title)
    }

    /**
     * Get start date of the event, store in start veriables for year, month and minutes
     */
    fun startDateButtonClicked(view: View) {
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                Log.i("EDIT", "Changing Start date: $year/$monthOfYear/$dayOfMonth")
                val startDateButton: Button = findViewById<Button>(R.id.startDateButton)
                startDateButton.text = "Start Date: $year/${monthOfYear+1}/$dayOfMonth"

            },
            startYear,
            startMonth,
            startDay
        )

        dpd.show()
    }

    /**
     *  Get end date of the event, store in start veriables for year, month and minutes
     */
    fun endDateButtonClicked(view: View) {
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                Log.i("EDIT", "Changing End date: $year/${monthOfYear+1}/$dayOfMonth")
                val endDateButton: Button = findViewById<Button>(R.id.endDateButton)
                endDateButton.text = "End Date: $year/${monthOfYear+1}/$dayOfMonth"

            },
            endYear,
            endMonth,
            endDay
        )

        dpd.show()
    }


    /**
     * Getting the start hour and minutes of the event
     */
    fun startTimeButtonClicked(view: View) {
        val tpd =
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->

                Log.i("EDIT", "Changing Start time: $h:$m")
                val startTimeButton: Button = findViewById<Button>(R.id.startTimeButton)
                startTimeButton.text = "Start Time: $h:$m"
                startHour=h
                startMinute=m
            }), startHour, startMinute, true)

        tpd.show()
    }

    /**
     * Getting the end hour and minute of the event.
     */
    fun endTimeButtonClicked(view: View) {
        val tpd =
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->

                Log.i("EDIT", "Changing End time: $h:$m")
                val endTimeButton: Button = findViewById<Button>(R.id.endTimeButton)
                endTimeButton.text = "End Time: $h:$m"
                endHour=h
                endMinute=m
            }), endHour, endMinute, true)

        tpd.show()
    }

    /**
     * Save change made to the event.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun saveChangesButtonClicked(view: View) {
        //start and end millis
        val startMillis: Long = java.util.Calendar.getInstance().run {
            set(startYear, startMonth, startDay, startHour, startMinute)
            timeInMillis
        }
        Log.i("EDIT","start time about to be inserted: $startYear/$startMonth/$startDay    $startHour:$startMinute")
        val endMillis: Long = java.util.Calendar.getInstance().run {
            set(endYear, endMonth, endDay, endHour, endMinute)
            timeInMillis
        }
        Log.i("EDIT","start time about to be inserted: $endYear/$endMonth/$endDay    $endHour:$endMinute")
        this.event.startDate = startMillis
        this.event.endDate = endMillis
        val descEditText: EditText = findViewById<EditText>(R.id.eventDescEditText)
        val titleEditText: EditText = findViewById<EditText>(R.id.eventTitleEditText)
        this.event.desc = descEditText.text.toString()
        this.event.title = titleEditText.text.toString()

        Log.i("EDIT", "Saved Edited Event: ${this.event.toString()}")
        this.eventManager.updateEvent(this.event)
        finish()
    }

    override fun onNothingSelected(parent: AdapterView<*>) {}
    /**
     * Setting importance from spinner.
     */
    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        this.event.importance=parent.getItemAtPosition(pos).toString()

    }

    fun saveReminderClicked(view: View) {
        val reminderEditText: EditText = findViewById<EditText>(R.id.eventReminderEditText)
        val reminder: Int = reminderEditText.text.toString().toInt()
        this.event.addRemind(reminder)
        reminderEditText.setText("Add Reminder")
        Toast.makeText(this, "Reminder Added:  $reminder mins", Toast.LENGTH_LONG).show()
    }
}