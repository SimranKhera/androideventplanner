package ca.dawson.planner

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CalendarView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_event_new.*
import kotlinx.android.synthetic.main.fragment_event_new.view.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [EventNewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EventNewFragment(contex: Context) : Fragment() {
    lateinit var event_importance: String
    var event_reminder: Int = 0
    private val TAG = "MyActivity"
    var sday = 0
    var smonth: Int = 0
    var syear: Int = 0
    var shour: Int = 0
    var sminute: Int = 0

    var eday = 0
    var emonth: Int = 0
    var eyear: Int = 0
    var ehour: Int = 0
    var eminute: Int = 0
    val c = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_new, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        event_sDate.setOnClickListener {
            this.onClickDate(view)
        }
        event_sTime.setOnClickListener {
            this.onClickTime(view)
        }
        event_eDate.setOnClickListener {
            this.onClickEndDate(view)
        }
        event_eTime.setOnClickListener {
            this.onClickEndTime(view)
        }

        event_Importance.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                event_importance = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                Toast.makeText(
                    activity,
                    "Please select the importance of the event",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        event_Reminder.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                event_reminder = Integer.parseInt(parent?.getItemAtPosition(position).toString())
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
        view.event_create?.setOnClickListener {
            createNewEvent()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun onClickDate(view: View?) {
        sday = c.get(Calendar.DAY_OF_MONTH)
        smonth = c.get(Calendar.MONTH)
        syear = c.get(Calendar.YEAR)
        val dpd = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener(function = { view, year, monthOfYear, dayOfMonth ->

                Toast.makeText(
                    activity,
                    "$dayOfMonth / ${monthOfYear + 1} / $year",
                    Toast.LENGTH_LONG
                ).show()

            }),
            syear,
            smonth,
            sday
        )

        dpd.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun onClickTime(view: View?) {
        Log.v(TAG, "clickDatePicker called1");

        shour = c.get(Calendar.HOUR)
        sminute = c.get(Calendar.MINUTE)
        Log.v(TAG, "clickDatePicker called")
        val tpd =
            TimePickerDialog(context, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                Log.v(TAG, "Function Time Picker started")
                Log.i("Edit", "Time" + h.toString() + ":" + m.toString())

                Toast.makeText(activity, h.toString() + " : " + m.toString(), Toast.LENGTH_LONG)
                    .show()
                shour = h
                sminute = m
            }), shour, sminute, true)
        tpd.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun onClickEndDate(view: View?) {
        eday = c.get(Calendar.DAY_OF_MONTH)
        emonth = c.get(Calendar.MONTH)
        eyear = c.get(Calendar.YEAR)
        val dpd = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener(function = { view, year, monthOfYear, dayOfMonth ->


                Toast.makeText(
                    activity,
                    "$dayOfMonth / ${monthOfYear + 1} / $year",
                    Toast.LENGTH_LONG
                ).show()

            }),
            eday,
            emonth,
            eyear
        )
        dpd.getDatePicker().setMinDate(System.currentTimeMillis());
        dpd.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun onClickEndTime(view: View?) {
        ehour = c.get(Calendar.HOUR)
        eminute = c.get(Calendar.MINUTE)
        Log.v(TAG, "clickDatePicker called")
        val tpd =
            TimePickerDialog(context, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                Log.v(TAG, "Function Time Picker started")
                Log.i("Edit", "Time" + h.toString() + ":" + m.toString())
                Toast.makeText(
                    activity,
                    h.toString() + " : " + m.toString(),
                    Toast.LENGTH_LONG
                ).show()
                ehour = h
                eminute = m
            }), ehour, eminute, true)
        tpd.show()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun createNewEvent() {
        val eventManager: EventManager = EventManager(activity!!)
        val title: String = event_title.text.toString()
        val startMillis: Long = java.util.Calendar.getInstance().run {
            set(syear, smonth, sday, shour, sminute)
            timeInMillis
        }
        val endMillis: Long = java.util.Calendar.getInstance().run {
            set(eyear, emonth, eday, ehour, eminute)
            timeInMillis
        }

        val description = event_Description.text.toString()

        var newEvent: Event =
            Event(title, startMillis, endMillis, description, event_importance, -1)
        Log.i("NEW", "New event: ${newEvent.toString()}")
        newEvent.addRemind(event_reminder)
        eventManager.addEvent(newEvent)

        Toast.makeText(activity, "New event created!", Toast.LENGTH_SHORT).show()
        clear()
    }

    fun clear(){
        event_title.setText("")
        event_Description.setText("")

    }
}