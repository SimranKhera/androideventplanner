package ca.dawson.planner

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.provider.CalendarContract
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import java.util.*

/**
 * Make sure dates are in milliseconds before calling any of these methods.
 * These are CRUD methods for the calendar app. Must be used in all fragments.
 * @author Simranjot Kaur Khera
 */
class EventManager(val context: Context) {


    //default calendar -> this calendar must exist
    private val calID: Long = 5

    /**
     * Default start and end dates.
     */
    val startMillis: Long = java.util.Calendar.getInstance().run {
        set(2020, 12, 14, 7, 30)
        timeInMillis
    }
    val endMillis: Long = java.util.Calendar.getInstance().run {
        set(2020, 12, 14, 8, 45)
        timeInMillis
    }

    /**
     * Adds event to calendar as well as its reminders.
     * When adding a new event, the eventid can be -1 -> it won't be used.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun addEvent(event: Event){
        Log.i("EVENT", "adding event");
        val values = ContentValues().apply {
            put(CalendarContract.Events.DTSTART, event.startDate)
            put(CalendarContract.Events.DTEND, event.endDate)
            put(CalendarContract.Events.TITLE, event.title)
            put(CalendarContract.Events.DESCRIPTION, event.desc.plus("{"+event.importance+"}").plus("*"))
            put(CalendarContract.Events.CALENDAR_ID, calID)
            put(CalendarContract.Events.EVENT_TIMEZONE, "America/Los_Angeles")
        }
        val uri: Uri? = context.contentResolver.insert(CalendarContract.Events.CONTENT_URI, values)
        event.id= uri!!.lastPathSegment!!.toLong()
        Log.i("EVENT", " event added " + uri!!.lastPathSegment.toString() + ": ${event.toString()}");
        //addReminders(event)
        setAlarms(event)
    }

    /**
     * Returns an event matching the name.
     */
    fun getEvent(name: String): Event{
        Log.i("EVENT", "getting event");
        // Run query
        val uri: Uri = CalendarContract.Events.CONTENT_URI
        val cursor: Cursor? = context.contentResolver.query(uri, null, null, null, null)
        while(cursor!!.moveToNext()){
            val eventName: String = cursor.getString(cursor.getColumnIndex(CalendarContract.Events.TITLE))
            val startDate: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTSTART))
            val endDate: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTEND))
            var desc: String =  cursor.getString(cursor.getColumnIndex(CalendarContract.Events.DESCRIPTION))
            val id: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events._ID))

            val importance = getImportance(desc)
            val start: Int = desc.indexOf('{')+1
            val end: Int = desc.indexOf('}')
            if(start!= 0){
                desc=desc.removeRange(start-1, end+1)
            }

            if(eventName == name){
                Log.i("EVENT", "event found $eventName");
                desc= desc.replace("*", "")
                return  return Event(eventName, startDate, endDate, desc, importance, id)
            }

        }
        cursor.close()
        Log.i("EVENT", "event retrieved");
        return Event("", startMillis, endMillis, "", "", -1)
    }

    /**
     * Returns all events.
     */
    fun getAllEvents():MutableList<Event> {
        Log.i("EVENT", "getting all events");
        val events = mutableListOf<Event>()
        // Run query
        val uri: Uri = CalendarContract.Events.CONTENT_URI
        val cursor: Cursor? = context.contentResolver.query(uri, null, null, null, "DTSTART DESC")
        while(cursor!!.moveToNext()){
            val eventName: String = cursor.getString(cursor.getColumnIndex(CalendarContract.Events.TITLE))
            val startDate: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTSTART))
            val endDate: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events.DTEND))
            var desc: String =  cursor.getString(cursor.getColumnIndex(CalendarContract.Events.DESCRIPTION))
            val id: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Events._ID))

            if(desc.contains("*")){
                val importance: String = getImportance(desc)
                val start: Int = desc.indexOf('{')+1
                val end: Int = desc.indexOf('}')
                if(start != 0){
                    desc=desc.removeRange(start-1, end+1)
                }

                desc=desc.replace("*", "")
                events.add(Event(eventName, startDate, endDate, desc, importance, id))
            }
        }
        cursor.close()
        Log.i("EVENT", "all events retrieved");
        return events
    }

    /**
     * Updates the event with the same id as the event provided.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun updateEvent(event: Event){
        val values = ContentValues().apply {
            put(CalendarContract.Events.DTSTART, event.startDate)
            put(CalendarContract.Events.DTEND, event.endDate)
            put(CalendarContract.Events.TITLE, event.title)
            put(CalendarContract.Events.DESCRIPTION,  event.desc.plus("{"+event.importance+"}").plus("*"))
            Calendar.PM

            put(CalendarContract.Events.CALENDAR_ID, calID)
            put(CalendarContract.Events.EVENT_TIMEZONE, "America/Los_Angeles")
        }
        val updateUri: Uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, event.id)
        val rows: Int = context.contentResolver.update(updateUri, values, null, null)
        setAlarms(event)
        Log.i("EVENT", "Rows updated: $rows")
    }

    /**
     * Get importance is used from getEvent and getAllEvents() when creating event objects.
     */
    private fun getImportance(desc: String): String{
        val start: Int = desc.indexOf('{')+1
        val end: Int = desc.indexOf('}')

        if(start != 0){
            var importance: String = desc.substring(start, end)
            Log.i("EVENT", "importance for event found: $importance" )
            return importance

        }
        return ""
    }

    /**
     * Uses alarmanager to set alarms for reminders. Alarm will show a toast
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun setAlarms(event: Event) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        for (reminder: Int in event.reminders){
            Log.i("ALARM", "setting alarm for: $reminder before start for event: ${event.toString()}")
            // Get AlarmManager instance

            // Intent part
            val intent = Intent(context, AlarmReceiver::class.java)
            intent.action = "FOO_ACTION"

            intent.putExtra("DESC", event.desc)
            intent.putExtra("START", event.getDate(event.startDate, "dd/MM/yyyy hh:mm"))
            intent.putExtra("TITLE", event.title)

            val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)

            // Alarm time
            val alarmTimeAtUTC: Long = (event.startDate - 60000*reminder)
            Log.i("ALARM", "Time before start: ${event.getDate(alarmTimeAtUTC, "dd/MM/yyyy hh:mm:ss.SSS")}")
            // Set with system Alarm Service
            // Other possible functions: setExact() / setRepeating() / setWindow(), etc
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTimeAtUTC, pendingIntent)
        }

    }


}