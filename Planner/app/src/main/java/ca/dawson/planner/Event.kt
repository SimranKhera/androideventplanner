package ca.dawson.planner

import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

/**
 * holds event data, when adding a new event the id can be set to -1 -> it will be set by the addEvent() method
 * in EventManager class
 * @author Simranjot Kaur Khera
 */
class Event(
    var title: String,
    var startDate: Long,
    var endDate: Long,
    var desc: String,
    var importance: String,
    var id: Long
) : Serializable {
    val reminders = mutableListOf<Int>(100)

    /**
     * Ask the user how many minutes before the event they want the reminder to occur.
     * Then add the minutes to the event. When creating the event, the reminders will be set as well from
     * EventManager.
     *
     */
    fun addRemind(reminderMinutes: Int) {
        this.reminders.add(reminderMinutes)
    }

    override fun toString(): String {
        return "Event(title='$title', reminders num: '${reminders.size}' , startDate=${
            getDate(
                startDate,
                "dd/MM/yyyy hh:mm:ss.SSS"
            )
        }, endDate=${
            getDate(
                endDate,
                "dd/MM/yyyy hh:mm:ss.SSS"
            )
        }, desc='$desc', importance='$importance', id=$id)"
    }

    fun getDate(milliSeconds: Long, dateFormat: String): String {
        val formatter = SimpleDateFormat(dateFormat)

        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(milliSeconds)
        return formatter.format(calendar.getTime())
    }
}