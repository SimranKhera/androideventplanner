package ca.dawson.planner

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.Fragment

/**
 * Represents the list of events currently in the calendar.
 * @author Simranjot Kaur Khera
 */

class EventListFragment(context: Context) : Fragment() {

    private var listView: ListView? = null
    private lateinit var listAdapter: Adapter
    private val eventManager = EventManager(context)
    private var firstTime:Int =2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_event_list, container, false)
        val allEvents = eventManager.getAllEvents()
        val listItems = arrayOfNulls<String>(allEvents.size)
        for (i in 0 until allEvents.size) {
            val event = allEvents[i]
            listItems[i] = event.title
        }
        listView = view.findViewById(R.id.listView)
        val adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, listItems)
        listAdapter = adapter
        listView!!.adapter = adapter
        listView!!.setOnItemClickListener { parent, view, position, id ->

            //getting event object from calendar.
            val elementName: String =
                adapter.getItem(position) as String// The item that was clicked
            val event: Event = eventManager.getEvent(elementName)

            //creating intent -> starting event edit activity.
            val i = Intent(context, EventEdit::class.java)

            i.putExtra("Event", event)
            startActivity(i)
            Log.i("EVENT", event.toString())

        }
        return view
    }

    /**
     * After adding an event, the list of events must be updated. this refreshes the fragment.
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(firstTime == 0){
            if (isVisibleToUser) {
                val allEvents = eventManager.getAllEvents()
                val listItems = arrayOfNulls<String>(allEvents.size)
                for (i in 0 until allEvents.size) {
                    val event = allEvents[i]
                    listItems[i] = event.title
                }
                if(context == null){
                    Log.i("LIST", "context is null")
                }

                val adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, listItems)
                listView!!.adapter = adapter

                listView!!.setOnItemClickListener { parent, view, position, id ->

                    //getting event object from calendar.
                    val elementName: String =
                        adapter.getItem(position) as String// The item that was clicked
                    val event: Event = eventManager.getEvent(elementName)

                    //creating intent -> starting event edit activity.
                    val i = Intent(context, EventEdit::class.java)

                    i.putExtra("Event", event)
                    startActivity(i)
                    Log.i("EVENT", event.toString())

                }
                Log.i("LIST", "list of events updated")

            }
        }else{
            firstTime--
            Log.i("LIST", "setting firstime to $firstTime")
        }

    }
}

