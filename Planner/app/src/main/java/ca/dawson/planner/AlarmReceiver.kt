package ca.dawson.planner

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity

/**
 * Received all alarms, displays a toast when reminder occurs.
 * @author Simranjot Kaur Khera
 */
class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        // Is triggered when alarm goes off, i.e. receiving a system broadcast
        if (intent.action == "FOO_ACTION") {
            Log.i("ALARM", "Alarming")
            val title: String? = intent.extras!!.getString("TITLE")
            val desc: String? = intent.extras!!.getString("DESC")
            val startDate: String? = intent.extras!!.getString("START")


            Log.i("ALARM", "Found event to ALARM: $title")
            Log.i("ALARM", "Found event to ALARM: $desc")
            Log.i("ALARM", "Found event to ALARM: $startDate")
            Toast.makeText(context, "Reminder: *$title* $desc at $startDate", Toast.LENGTH_LONG )

        }
    }
}