package ca.dawson.planner

import android.accessibilityservice.GestureDescription
import android.content.ContentResolver
import android.content.ContentValues
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import java.util.*

/**
 * Sets up tabs
 * @author Simranjot Kaur Khera
 */

class MainActivity : AppCompatActivity() {
    val eventManager: EventManager = EventManager(this)
    val startMillis: Long = java.util.Calendar.getInstance().run {
        set(2020, 11, 14, 4, 5)
        timeInMillis
    }
    val endMillis: Long = java.util.Calendar.getInstance().run {
        set(2020, 11, 14, 13, 0)
        timeInMillis
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupTabs()
    }


    /**
     * Used to find all calendars and ids. Used for debugging in case the program doesn't work.
     */
    fun findCalendars() {
        Log.i("Cal", "finding cal")
        val projection =
            arrayOf("_id", "calendar_displayName")
        val calendars: Uri
        calendars = Uri.parse("content://com.android.calendar/calendars")

        val contentResolver: ContentResolver = this.contentResolver
        val managedCursor =
            contentResolver.query(calendars, projection, null, null, null)

        if (managedCursor!!.moveToFirst()) {

            var calName: String?
            var calID: String?
            var cont = 0
            val nameCol = managedCursor!!.getColumnIndex(projection[1])
            val idCol = managedCursor!!.getColumnIndex(projection[0])
            do {
                calName = managedCursor!!.getString(nameCol)
                calID = managedCursor!!.getString(idCol)
                Log.i("Cal", "Calendar found: " + calName + " " + calID)
                cont++
            } while (managedCursor!!.moveToNext())
            managedCursor!!.close()
        }
    }

    /**
     * Sets up the fragments tabs
     */
    fun setupTabs() {
        val tabLayout: TabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager: ViewPager = findViewById<ViewPager>(R.id.viewPager)

        tabLayout.addTab(tabLayout.newTab().setText(R.string.first_tab_name))
        tabLayout.addTab(tabLayout.newTab().setText(R.string.second_tab_name))

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val pagerAdapt: PagerAdapter = PagerAdapter(supportFragmentManager, this)
        viewPager.adapter = pagerAdapt
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }
}